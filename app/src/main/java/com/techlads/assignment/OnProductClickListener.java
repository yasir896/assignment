package com.techlads.assignment;

import android.view.View;

import java.util.ArrayList;
import java.util.List;


public interface OnProductClickListener {

    public void onClick(View view, int position, List<Product> products);
    public void onCountUpdateClick(int position , Product product);
}
