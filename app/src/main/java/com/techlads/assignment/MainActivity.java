package com.techlads.assignment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.carteasy.v1.lib.Carteasy;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnProductClickListener {

    int LOADING_TIME_OUT = 4000;

    private RecyclerView recyclerView;
    private TextView counterTv;
    private RecyclerView.LayoutManager layoutManager;
    private ProductsAdapter productsAdapter;
    private List<Product> mProducts = new ArrayList<>();
    private ProgressDialog progress;

    private AlertDialog addToCartDialog;
    private Carteasy cs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.products_rc);
        counterTv = findViewById(R.id.counterTv);

        findViewById(R.id.ic_shoppingCart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this , CartActivity.class));
            }
        });

        //showing progress dialog while updating list
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();


        cs = new Carteasy();
        cs.persistData(getApplicationContext(), true);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                recyclerView.setHasFixedSize(true);
                layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
//                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
                productsAdapter = new ProductsAdapter(mProducts, MainActivity.this);
                recyclerView.setAdapter(productsAdapter);

                prepareProducts();

                if( progress != null && progress.isShowing()){
                    progress.hide();
                }

            }
        }, LOADING_TIME_OUT);


    }


    private void prepareProducts() {

        Product pr = new Product("Code Complete, By Steve McConnell", "Books", 150);
        mProducts.add(pr);
        pr = new Product("The Complete Reference – C++, By Herbert Schildt", "Books", 250);
        mProducts.add(pr);
        pr = new Product("Learning to Program, By Stephen Foote", "Books", 85);
        mProducts.add(pr);
        pr = new Product("Programming Pearls by Jon Bentley", "Books", 95);
        mProducts.add(pr);
        pr = new Product("The Pragmatic Programmer: From Journeyman to Master", "Books", 100);
        mProducts.add(pr);
        pr = new Product("Structure and Interpretation of Computer Programs", "Books", 156);
        mProducts.add(pr);
        pr = new Product("Beginning Programming for Dummies, By Wallace & Wally Wang", "Books", 50);
        mProducts.add(pr);
        pr = new Product("Absolute Beginner’s Guide to C", "Books", 50);
        mProducts.add(pr);
        pr = new Product("Java: A Beginner’s Guide, Sixth Edition", "Books", 65);
        mProducts.add(pr);

        productsAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view, int position, List<Product> products) {

        Product product = mProducts.get(position);
        displayCartView(product);
    }

    @Override
    public void onCountUpdateClick(int position, Product product) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCounter();
    }

    public void displayCartView(final Product product){

        final View mCartView = getLayoutInflater().inflate(R.layout.add_to_cart_view, null);
        addToCartDialog = new AlertDialog.Builder(MainActivity.this).create();
        addToCartDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        addToCartDialog.setView(mCartView);
        addToCartDialog.setCanceledOnTouchOutside(true);
        addToCartDialog.show();

        TextView productName = mCartView.findViewById(R.id.product_name_tv);
        TextView productPrice = mCartView.findViewById(R.id.product_price_tv);
        TextView closeDialog = mCartView.findViewById(R.id.cancel_tv);
        TextView addtoCart = mCartView.findViewById(R.id.add_to_cart_tv);

        productName.setText(product.getName());
        productPrice.setText("$"+ product.getPrice());



        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToCartDialog.dismiss();
            }
        });

        addtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                product.setQuantity(((CounterWidget) mCartView.findViewById(R.id.counterCw)).getCounter());
                cs.add(product.getName() , product.getName() , new Gson().toJson(product));
                cs.commit(getApplicationContext());
                Toast.makeText(MainActivity.this, "Product added successfully !!", Toast.LENGTH_SHORT).show();
                addToCartDialog.dismiss();


                updateCounter();
            }
        });

    }

    private void updateCounter() {
        counterTv.setText(String.valueOf(cs.ViewAll(getApplicationContext()).size()));
    }

}
