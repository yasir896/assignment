package com.techlads.assignment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.carteasy.v1.lib.Carteasy;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CartActivity extends AppCompatActivity {

    int LOADING_TIME_OUT = 4000;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ProductsAdapter productsAdapter;
    private List<Product> mProducts = new ArrayList<>();
    private ProgressDialog progress;

    private Carteasy cs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        recyclerView = findViewById(R.id.products_rc);
        findViewById(R.id.confirmBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext() , UserDetailsActivity.class));
            }
        });

//        showing progress dialog while updating list
        progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
//                recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL));
        productsAdapter = new ProductsAdapter(mProducts, new OnProductClickListener() {
            @Override
            public void onClick(View view, int position, List<Product> products) {

            }

            @Override
            public void onCountUpdateClick(int position, Product product) {
                recalculate();
            }
        });
        recyclerView.setAdapter(productsAdapter);

        productsAdapter.setShowCounter(true);

        cs = new Carteasy();
        Map<Integer, Map> data;

        data = cs.ViewAll(getApplicationContext());


        for (Map.Entry<Integer, Map> entry : data.entrySet()) {

            Map<String, String> innerdata = entry.getValue();
            for (Map.Entry<String, String> innerentry : innerdata.entrySet()) {

                Product product = new Gson().fromJson(String.valueOf(innerentry.getValue()), Product.class);

                mProducts.add(product);

            }
        }




        productsAdapter.notifyDataSetChanged();
        recalculate();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if( progress != null && progress.isShowing()){
                    progress.hide();
                }

            }
        }, LOADING_TIME_OUT);
//

    }

    private void recalculate() {
        int totalCount = 0;

        for (Product product : mProducts) {
            totalCount += product.getTotalPrice();

        }
        ((TextView) findViewById(R.id.totalTv)).setText("$" + totalCount);

    }


}
