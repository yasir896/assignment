package com.techlads.assignment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class UserDetailsActivity extends AppCompatActivity {

    private EditText userNameEt;
    private EditText phoneNumberEt;
    private EditText emailEt;
    Button submitButton;

    private View confirmationVIew;
    private AlertDialog addToCartDialog;
    Button smsButton;
    Button emailButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        userNameEt = (EditText) findViewById(R.id.userName_Et);
        phoneNumberEt = (EditText) findViewById(R.id.phoneNumber_Et);
        emailEt = (EditText) findViewById(R.id.email_Et);

        submitButton = (Button) findViewById(R.id.submit_btn);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mName = userNameEt.getText().toString().trim();
                String mPhone = phoneNumberEt.getText().toString().trim();
                String mEmail = emailEt.getText().toString().trim();

                if (TextUtils.isEmpty(mName)){
                    userNameEt.setError("Name is Required");
                }
                else if (TextUtils.isEmpty(mPhone)){
                    phoneNumberEt.setError("Phone Number is Required");
                }
                else if (TextUtils.isEmpty(mEmail)){
                    emailEt.setError("Email is Required.");
                }
                else {
                    //Toast.makeText(UserDetailsActivity.this,
                    //        "Do you want to confirm your Order?", Toast.LENGTH_SHORT).show();

                    String message = "Dear " + mName + " Your Order has been placed.";
                    //sendEmail(mEmail, message);
                    displayConfirmationDialog(mPhone, mEmail, message);
                }
            }
        });

    }

    private void displayConfirmationDialog(final String number, final String email, final String Message){
        confirmationVIew = getLayoutInflater().inflate(R.layout.confirmation_view, null);
        addToCartDialog = new AlertDialog.Builder(UserDetailsActivity.this).create();
        addToCartDialog.setView(confirmationVIew);
        addToCartDialog.setCanceledOnTouchOutside(false);
        addToCartDialog.show();

        smsButton = confirmationVIew.findViewById(R.id.sms_btn);
        emailButton = confirmationVIew.findViewById(R.id.email_btn);

        smsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSms(number, Message);
            }
        });

        emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(email, Message);
            }
        });


    }



    private void sendEmail( String email, String message){

        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, email);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Confirmation");
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending...", "email");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(UserDetailsActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendSms(String phone, String message){
        try {
            Uri smsUri= Uri.parse("smsto:" + phone);
            Intent intent = new Intent(Intent.ACTION_VIEW, smsUri);
            intent.putExtra("address",phone);
            intent.putExtra("sms_body", message);
            intent.setType("vnd.android-dir/mms-sms");
            startActivity(intent);

        } catch (Exception ex){
            ex.printStackTrace();
        }
    }


}
