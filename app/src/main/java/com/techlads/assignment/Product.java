package com.techlads.assignment;


public class Product {

    String Name;
    String Category;
    int Price;

    int quantity;

    public Product(String name, String category, int price) {
        Name = name;
        Category = category;
        Price = price;

    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalPrice(){
        return getPrice() * getQuantity();
    }
}
