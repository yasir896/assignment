package com.techlads.assignment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {

    private final List<Product> mList;
    private boolean showCounter = false;

    public void setShowCounter(boolean showCounter) {
        this.showCounter = showCounter;
    }

    OnProductClickListener productClickListener;

    public ProductsAdapter(List<Product> mList, OnProductClickListener listener) {
        this.mList = mList;
        this.productClickListener = listener;
    }

    @NonNull
    @Override
    public  ProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.product_item, parent, false);

        return new ProductsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProductsViewHolder holder, final int pos) {

        final Product product = mList.get(pos);

        holder.nameTv.setText(product.getName());
        holder.categoryTv.setText(product.getCategory());
        holder.priceTv.setText("$"+ product.getPrice());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context, mList.get(pos).getName(), Toast.LENGTH_SHORT).show();
                if (productClickListener != null)
                productClickListener.onClick(holder.mView, pos, mList);
            }
        });

        if (showCounter){
            holder.counterCw.setVisibility(View.VISIBLE);
            holder.counterCw.setCounter(product.getQuantity());

            holder.counterCw.setListener(new CounterWidget.OnCounterChangeListener() {
                @Override
                public void OnCounterChange(int counter) {
                    product.setQuantity(counter);

                    if (productClickListener != null)
                        productClickListener.onCountUpdateClick(pos , product);
                }
            });
        }else {

            holder.counterCw.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ProductsViewHolder extends RecyclerView.ViewHolder{
        public final View mView;
        public final TextView nameTv;
        public final TextView categoryTv;
        public final TextView priceTv;
        public final CounterWidget counterCw;

        public ProductsViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;
            nameTv = itemView.findViewById(R.id.name_tv);
            categoryTv = itemView.findViewById(R.id.category_tv);
            priceTv = itemView.findViewById(R.id.price_tv);
            counterCw = itemView.findViewById(R.id.counterCw);
        }
    }
}
