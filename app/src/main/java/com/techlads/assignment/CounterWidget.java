package com.techlads.assignment;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;


public class CounterWidget extends LinearLayout {

    private int counter = 0;
    private OnCounterChangeListener listener;

    public void setListener(OnCounterChangeListener listener) {
        this.listener = listener;
    }

    public CounterWidget(Context context) {
        super(context);
        init();
    }

    public CounterWidget(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CounterWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CounterWidget(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void setCounter(int counter){
        if (counter < 0)
            return;

        this.counter = counter;

        ((TextView) findViewById(R.id.counterTv)).setText(String.valueOf(counter));

        if (listener != null){
            listener.OnCounterChange(counter);
        }
    }

    public int getCounter() {
        return counter;
    }

    public void init(){

        inflate(getContext() , R.layout.counter_layout , this);
        findViewById(R.id.lessBtn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                setCounter(counter - 1);
            }
        });

        findViewById(R.id.moreBtn).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                setCounter(counter + 1);
            }
        });
    }

    public interface OnCounterChangeListener {
        void OnCounterChange(int counter);
    }
}
